# locale

Patterns for matching things that commonly vary by locale (ref: Unix locale
system), such as currency values and number formatting.

## Usage

To use these patterns, clone or download the repository.  If the are stored in
your local file system in directory `locale`, you can load these patterns
explicitly on the command line, or have Rosie automatically load them as needed.

Explicit loading of a locale file:

    rosie -f <path>/locale/en_US.rpl grep en_US.lc_monetary <data_file>
	
Automatic loading of a locale file requires a setup step in which you tell Rosie
that to search the `locale` directory for RPL packages.  This bash command will
add the needed line to your **.rosierc** file:

    echo 'libpath = "<path>/locale"' >> ~/.rosierc
	
Note: If this is the first time you are using **~/.rosierc** (or the first time
setting **libpath** there) then follow the steps to
[set up .rosierc](#set-up-a-rosie-init-file) below.

Once you have set **libpath** to include the locale directory , the Rosie CLI
will search there automatically for packages to import, e.g.

    echo '$10.55' | rosie match en_US.lc_monetary


## Set up a Rosie init file

By default, Rosie loads **~/.rosierc** when the CLI starts up.  If you do not
have this init file, or it does not currently set **libpath**, then 
**you must also add the standard RPL library
directory** to your libpath.  _(Rationale: You are now creating a list of
directories for Rosie to search, in order, for RPL files. To have complete
control, you can say where in that list Rosie should look for standard RPL
libraries.)_ 

The standard RPL pattern library is usually `/usr/local/lib/rosie/rpl` unless
Rosie installed in a non-standard place.  You can always find the standard
library using the `rosie config` command and looking for `ROSIE_LIBDIR`, e.g.

    rosie config | rosie match -o subs '~ "ROSIE_LIBDIR" find:os.path'

_(That pattern reads: skip whitespace (~), match ROSIE\_LIBDIR, then find
`os.path`.  And `-o subs` sets the output mode to "sub matches of the overall
pattern".)_ 

Here's a one-line bash command to create a new **.rosierc** file:

    echo libpath = \"`rosie config | rosie match -o subs '~ "ROSIE_LIBDIR" find:os.path'`\" > ~/.rosierc


